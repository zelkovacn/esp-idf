/*
 * cmd_blecent.c
 *
 *  Created on: Aug 23, 2019
 *      Author: tom
 */
#include <stdio.h>
#include "argtable3/argtable3.h"
#include "host/ble_hs.h"
#include "esp_console.h"
#include "esp_log.h"
#include "cmd_blecent.h"

static const char *tag = "cmd_blecent";

pfn_blescan ble_scan = NULL;
pfn_bleconnect ble_connect = NULL;

static struct {
    struct arg_str *op_name;
    struct arg_end *end;
} blescan_args;

static int do_ble_scan_cmd(int argc, char **argv)
{
    int nerrors = arg_parse(argc, argv, (void **)&blescan_args);
    if (nerrors != 0) {
        arg_print_errors(stderr, blescan_args.end, argv[0]);
        return 1;
    }

	if (!strncmp(blescan_args.op_name->sval[0], "start", 5)) {
		if (ble_scan) {
			ble_scan();
		}
	} else if (!strncmp(blescan_args.op_name->sval[0], "stop", 4)) {
		ble_gap_disc_cancel();
	}
    return 0;
}

static void register_blecent_scan(void)
{
	blescan_args.op_name = arg_str1(NULL, NULL, "<start or stop>", "start or stop scan ble devices");
	blescan_args.end = arg_end(1);
    const esp_console_cmd_t ble_scan_cmd = {
        .command = "scan",
        .help = "start or stop scan BLE devices",
        .hint = NULL,
        .func = &do_ble_scan_cmd,
        .argtable = &blescan_args
    };
    ESP_ERROR_CHECK(esp_console_cmd_register(&ble_scan_cmd));
}

static struct {
    struct arg_str *addr;
    struct arg_end *end;
} bleconnect_args;

static int do_ble_connect_cmd(int argc, char **argv) {
	int nerrors = arg_parse(argc, argv, (void **) &bleconnect_args);
	if (nerrors != 0) {
		arg_print_errors(stderr, bleconnect_args.end, argv[0]);
		return 1;
	}
	ESP_LOGI(tag, "target ble device: %s", bleconnect_args.addr->sval[0]);
	ble_addr_t addr;
	sscanf(bleconnect_args.addr->sval[0], "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx",
			&addr.val[5], &addr.val[4], &addr.val[3], &addr.val[2], &addr.val[1], &addr.val[0]);
	if(ble_connect){
		addr.type = BLE_OWN_ADDR_RANDOM;
		ble_gap_disc_cancel();
		ble_connect(&addr);
	}
	return 0;
}

static void register_blecent_connect(void) {
	bleconnect_args.addr = arg_str1(NULL, NULL, "<addr>", "connect ble device with address");
	bleconnect_args.end = arg_end(1);
	const esp_console_cmd_t ble_scan_cmd = {
			.command = "connect",
			.help = "connect ble device with address",
			.hint = NULL,
			.func = &do_ble_connect_cmd,
	        .argtable = &bleconnect_args
	};
	ESP_ERROR_CHECK(esp_console_cmd_register(&ble_scan_cmd));
}

void register_blecent_tool(void) {
	register_blecent_scan();
	register_blecent_connect();
}

void register_ble_scan(pfn_blescan scan) {
	ble_scan = scan;
}

void register_ble_connect(pfn_bleconnect connect){
	ble_connect = connect;
}
