/*
 * cmd_blecent.h
 *
 *  Created on: Aug 23, 2019
 *      Author: tom
 */
#pragma once

#ifdef __cplusplus
extern "C" {
#endif

typedef void (*pfn_blescan)(void);

typedef void (*pfn_bleconnect)(const ble_addr_t *peer_addr);

void register_blecent_tool(void);

void register_ble_scan(pfn_blescan scan);

void register_ble_connect(pfn_bleconnect connect);


#ifdef __cplusplus
}
#endif
